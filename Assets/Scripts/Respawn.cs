using UnityEngine;

public class Respawn : MonoBehaviour
{
    public Transform respawnPoint;

    private void OnCollisionEnter(Collision collision)
    {
        // Verifica si el objeto ha colisionado con una superficie.
        if (collision.gameObject.tag == "Surface")
        {
            return;
        }

        // Mueve el objeto al punto de respawn.
        transform.position = respawnPoint.position;
    }
}