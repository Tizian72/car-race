using UnityEngine;
using UnityEngine.UI;

public class RaceController : MonoBehaviour
{
    public int totalLaps = 1; // total de vueltas para completar la carrera
    public Text lapCounterText; // Texto para mostrar el n�mero de vueltas
    public Text raceResultText; // Texto para mostrar el resultado final
    private int currentLap = 1; // n�mero de vuelta actual
    private bool raceFinished = false; // indica si la carrera ha terminado

    // se llama cada vez que el auto atraviesa el collider del punto de control
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !raceFinished)
        {
            if (currentLap < totalLaps) // si a�n no se han completado todas las vueltas
            {
                currentLap++;
                lapCounterText.text = "Lap " + currentLap + " / " + totalLaps;
            }
            else // si se han completado todas las vueltas
            {
                raceFinished = true;
                raceResultText.text = "�Has completado la carrera!";
            }
        }
    }
}
