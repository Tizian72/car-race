using UnityEngine;

public class RespawnController : MonoBehaviour
{
    public Transform respawnPoint;
    public string respawnTag = "RespawnSurface";

    private void OnCollisionEnter(Collision collision)
    {
        // Verifica si el objeto ha colisionado con una superficie.
        if (collision.gameObject.CompareTag(respawnTag))
        {
            // Mueve el objeto al punto de respawn.
            transform.position = respawnPoint.position;
        }
    }
}