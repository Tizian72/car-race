using UnityEngine;

public class CarMovement : MonoBehaviour
{
    public float speed = 10f; // Velocidad del auto
    public float rotationSpeed = 100f; // Velocidad de rotaci�n del auto

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal"); // Obtenemos la entrada horizontal
        float verticalInput = Input.GetAxis("Vertical"); // Obtenemos la entrada vertical

        // Movemos el auto hacia adelante o atr�s
        rb.velocity = transform.forward * verticalInput * speed;

        // Rotamos el auto a la izquierda o a la derecha
        transform.Rotate(Vector3.up, horizontalInput * rotationSpeed * Time.deltaTime);
    }
}