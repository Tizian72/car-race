using UnityEngine;

public class CarController : MonoBehaviour
{

    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    public float driftFactor = 0.95f;
    public float maxDrift = 2.0f;

    private float drift;

    void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        transform.Translate(0, 0, translation * Time.deltaTime);
        transform.Rotate(0, rotation * Time.deltaTime, 0);

        // Derrape
        if (Input.GetKey(KeyCode.Space))
        {
            drift = Mathf.Min(drift + Time.deltaTime, maxDrift);
            transform.Rotate(0, rotation * Time.deltaTime * drift, 0);
        }
        else
        {
            drift *= driftFactor;
        }

    }
}