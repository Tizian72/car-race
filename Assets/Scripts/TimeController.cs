using UnityEngine;
using UnityEngine.UI;

public class TimeController : MonoBehaviour
{
    public float currentTime = 0f;
    public float totalTime = 0f;
    public Text timeText;
    private bool raceFinished = false;

    void Start()
    {
        currentTime = 0f;
        timeText.text = "Time: 0.00";
    }

    void Update()
    {
        if (!raceFinished)
        {
            currentTime += Time.deltaTime;
            timeText.text = "Time: " + currentTime.ToString("F2");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("FinishLine"))
        {
            totalTime = currentTime;
            timeText.text = "Total Time: " + totalTime.ToString("F2");
            raceFinished = true;
        }
    }
}